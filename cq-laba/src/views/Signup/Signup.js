import React, {Component} from 'react';
import {PostData} from '../../services/api/postdata';
import {Redirect} from 'react-router-dom';
import './Signup.css';

class Signup extends Component {

  constructor(props){
    super(props);
   
    this.state = {
     userName: '',
     password: '',
     email: '',
    };

    this.signup = this.signup.bind(this);
    this.onChange = this.onChange.bind(this);
  }
 

  signup() {
    if(this.state.userName && this.state.password && this.state.email){
    PostData('signUp',this.state).then((result) => {
      let responseJson = result;
        console.log([result.message]);
        alert([result.message]);


        if(responseJson.userData){
        sessionStorage.setItem('userData',JSON.stringify(responseJson));

      }
      
     });
    }
  }

 onChange(e){
   this.setState({[e.target.name]:e.target.value});
  }

  render() {
    if  (sessionStorage.getItem('userData')) {
      return (<Redirect to={'/home'}/>)
    }
   
  

    return (
      
      <div className="row " id="Body">
        <div className="medium-5 columns left">
        <h4>Signup</h4>
        <label>Email</label>
        <input type="text" name="email"  placeholder="Email" onChange={this.onChange}/>

        <label>Username</label>
        <input type="text" name="userName" placeholder="Username" onChange={this.onChange}/>
        <label>Password</label>
        <input type="password" name="password"  placeholder="Password" onChange={this.onChange}/>
        
        <input type="submit" className="button" value="Sign Up" onClick={this.signup}/>
        <a href="/login">Login</a>
        </div>

      </div>
    );
  }
}

export default Signup;