import IndexPage from './IndexPage'
import MainPage from './MainPage'
import ReduxPage from './ReduxPage'
import AntdPage from './AntdPage'
import RestPage from './RestPage'
import FormPage from './FormPage'
import Welcome from './Welcome'
import Login from './Login'
import Signup from './Signup'
import TodosContainer  from './TodosContainer'

export {
  IndexPage,
  MainPage,
  ReduxPage,
  AntdPage,
  RestPage,
  FormPage,
  Welcome,
  Login,
  Signup,
  TodosContainer
}
