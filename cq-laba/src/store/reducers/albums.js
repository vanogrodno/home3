import {
  ALBUMS_FETCHING,
  ALBUMS_FAIL,
  ALBUMS_SUCCESS,

  CREATE_ALBUM_SUCCESS,
  CREATE_ALBUM_FAIL
} from '../types'

const initialState = {
  list: [],
  isFetching: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ALBUMS_FETCHING:
      debugger;
      return {
        ...state,
        isFetching: action.payload
      }
    case ALBUMS_SUCCESS:
      debugger;
      return {
        ...state,
        list: action.payload,
        isFetching: true
      }
    case ALBUMS_FAIL:
      debugger;
      return {
        ...state,
        isFetching: false
      }

    case CREATE_ALBUM_SUCCESS:
      debugger;
      return {
        ...state,
        list: [
          ...state.list,
          action.payload
        ]
      }

    case CREATE_ALBUM_FAIL:
      debugger;
      return {
        ...state,
        isFetching: false
      }

    default:
      return state
  }
}
