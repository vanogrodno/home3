import {
    MYUSERS_FAIL,
    MYUSERS_FETCHING,
    MYUSERS_SUCCESS,

    CREATE_MYUSERS_FAIL,
    CREATE_MYUSERS_SUCCESS
} from '../types'

const initialState = {
    list: [],
    isFetching: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case MYUSERS_FETCHING:
            debugger;
            return {
                ...state,
                isFetching: action.payload
            }
        case MYUSERS_SUCCESS:
            debugger;
            return {
                ...state,
                list: action.payload,
                isFetching: true
            }
        case MYUSERS_FAIL:
            debugger;
            return {
                ...state,
                isFetching: false
            }

        case CREATE_MYUSERS_SUCCESS:
            debugger;
            return {
                ...state,
                list: [
                    ...state.list,
                    action.payload
                ]
            }

        case CREATE_MYUSERS_FAIL:
            debugger;
            return {
                ...state,
                isFetching: false
            }

        default:
            return state
    }
}
