import {
  IP_LIST,
  IP_ADD,
  IP_DELETE,
  IP_UPDATE
} from '../types'

const initialState = {
  list: [{
    ip: '127.0.0.1',
    label: 'localhost'
  }]
}

export default (state = initialState, action) => {
  switch (action.type) {
    case IP_LIST:
      return state
    case IP_ADD:
      return {
        ...state,
        list: [
          ...state.list,
          action.payload
        ]
      }
    case IP_UPDATE:
      const index = state.list.findIndex(i => i.ip === action.payload.ip)
      return {
        ...state,
        list: [
          ...state.list.slice(0, index),
          action.payload,
          ...state.list.slice(index + 1)
        ]
      }
    case IP_DELETE:
      console.log(['Delete to be implemented'])
      return {
        ...state,
        list: state.list.filter(i => i.ip !== action.payload.ip)
      }
    default:
      return state
  }
}
