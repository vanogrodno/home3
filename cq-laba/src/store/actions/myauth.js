import {
    MYUSERS_FAIL,
    MYUSERS_FETCHING,
    MYUSERS_SUCCESS,

    CREATE_MYUSERS_FAIL,
    CREATE_MYUSERS_SUCCESS
} from '../types'

import { api } from '../../services'

const signUp = (data) => dispatch => {
    dispatch({
        type: MYUSERS_FETCHING,
        payload: true
    })
    return api.myauth.signUp(data)
        .then(({ data }) => {
            if (data) {
                dispatch({
                    type: CREATE_MYUSERS_SUCCESS,
                    payload: data
                })
            }
            dispatch({
                type: MYUSERS_FETCHING,
                payload: false
            })
        }, (data) => {
            dispatch({
                type: CREATE_MYUSERS_FAIL,
                payload: data
            })
        })
}


export {
    signUp
}
